# vac_ctrl_tcp350 conda recipe

Home: "https://gitlab.esss.lu.se/epics-modules/vac_ctrl_tcp350"

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS vac_ctrl_tcp350 module
